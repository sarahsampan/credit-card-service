package com.compareglobal.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;

import com.compareglobal.service.creditcard.model.Compare;

public class RulesTest {

	@Test
    public void testRuleMessage() {
		
		String drl =
		"package com.compareglobal.service; "+
		"import com.compareglobal.service.creditcard.model.Compare; "+
		"dialect 'mvel' "+
		"global java.util.List list; "+
		"rule compare "+
		"no-loop "+
		"when "+
		    "$compare :Compare(); "+
		 "then "+
	 		"$compare.findCompareQueryString(); "+
			"update($compare); "+
			"list.add($compare.getQuery()); "+
			"System.out.println('Rules Name is - ' + drools.getRule().getName()); "+
		 "end ; ";
		
		KieHelper helper = new KieHelper();
	    helper.addContent( drl, ResourceType.DRL );
	    KieSession kieSession = helper.build().newKieSession();
	        
		List list = new ArrayList();
		kieSession.setGlobal( "list", list );

		Compare comp = new Compare();
		comp.setLanguage("en");
		comp.setKey("CASHBACK");
		kieSession.insert(comp);
		
		kieSession.fireAllRules();
		String result = null;
		for (int i=0; i< list.size(); i++){
			result = (String) list.get(i);
		}
		
		assertNotNull(result);
	}
	
}
