package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Fee {

    private String id;
    private String creditCardId;
    private String annualFeeId;
    private String otherFeeId;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public Fee() {
        super();
    }

    public Fee(
    		final String creditCardId, 
    		final String annualFeeId, 
    		final String otherFeeId, 
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setCreditCardId(creditCardId);
        this.setAnnualFeeId(annualFeeId);
        this.setOtherFeeId(otherFeeId);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setCreditCardId(String creditCardId) {
	   this.creditCardId = creditCardId;
   }
	   
   public String getCreditCardId() {
	   return creditCardId;
   }
   
   public void setAnnualFeeId(String annualFeeId) {
	   this.annualFeeId = annualFeeId;
   }
   
   public String getAnnualFeeId() {
	   return annualFeeId;
   }
   
   public void setOtherFeeId(String otherFeeId) {
	   this.otherFeeId = otherFeeId;
   }
   
   public String getOtherFeeId() {
	   return otherFeeId;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
       return "Fee [id=" + id + ", creditCardId=" + creditCardId
           + ", annualFeeId=" + annualFeeId + ", otherFeeId=" + otherFeeId
           + ", created=" + created + ", modified=" + modified
           + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy + "]";
   }
}