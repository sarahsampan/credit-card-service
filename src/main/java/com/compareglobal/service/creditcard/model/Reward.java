package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class Reward {

    private String id;
    private String creditCardId;
    private String airmileId;
    private String shopping;
    private String entertainment;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
    private AirMile airMile;
   
    public Reward() {
        super();
    }

    /**
     * @param creditCardId
     * @param airmileId
     * @param shopping
     * @param entertainment
     * @param created
     * @param modified
     * @param createdBy
     * @param modifiedBy
     */
    public Reward( 
    		final String creditCardId,
    		final String airmileId,
    		final String shopping,
    		final String entertainment,
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
    	this.setCreditCardId(creditCardId);
    	this.setAirmileId(airmileId);
    	this.setShopping(shopping);
    	this.setEntertainment(entertainment);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }

   /**
	* @return the id
	*/
   public void setId(String id) {
       this.id = id;
   }

   /**
    * @param id the id to set
    */
   public String getId() {
       return id;
   }

   /**
	* @return the creditCardId
	*/
   public String getCreditCardId() {
	   return creditCardId;
   }

   /**
    * @param creditCardId the creditCardId to set
    */
   public void setCreditCardId(String creditCardId) {
	   this.creditCardId = creditCardId;
   }

   /**
    * @return the airmileId
    */
   public String getAirmileId() {
	   return airmileId;
   }

   /**
    * @param airmileId the airmileId to set
    */
   public void setAirmileId(String airmileId) {
	   this.airmileId = airmileId;
   }

   /**
    * @return the shopping
    */
   public String getShopping() {
	   return shopping;
   }

   /**
     * @param shopping the shopping to set
     */
   public void setShopping(String shopping) {
	   this.shopping = shopping;
   }

   /**
    * @return the entertainment
    */
   public String getEntertainment() {
	   return entertainment;
   }

   /**
    * @param entertainment the entertainment to set
    */
   public void setEntertainment(String entertainment) {
	   this.entertainment = entertainment;
   }

   /**
    * @return the created
    */
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   /**
    * @param created the created to set
    */
   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   /**
    * @return the modified
    */
   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   /**
    * @param modified the modified to set
    */
   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   /**
    * @return the createdBy
    */
   public String getCreatedBy() {
	   return createdBy;
   }

   /**
    * @param createdBy the createdBy to set
    */
   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   /**
    * @return the modifiedBy
    */
   public String getModifiedBy() {
	   return modifiedBy;
   }

   /**
    * @param modifiedBy the modifiedBy to set
    */
   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }
   
   public AirMile getAirMile() {
	   return airMile;
   }
	
   public void setAirMile(AirMile airMile) {
	   this.airMile = airMile;
   }

	@Override
   public String toString() {
	   return "Reward [id=" + id + ", creditCardId=" + creditCardId
			+ ", airmileId=" + airmileId + ", shopping=" + shopping
			+ ", entertainment=" + entertainment + ", created=" + created
			+ ", modified=" + modified + ", createdBy=" + createdBy
			+ ", modifiedBy=" + modifiedBy + "]";
   }
}