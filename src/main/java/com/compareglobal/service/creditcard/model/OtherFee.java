package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class OtherFee {

    private String id;
    private String foreignTransactionFee;
    private Double overLimit;
    private Double cardReplacement;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public OtherFee() {
        super();
    }

    public OtherFee(
    		final String foreignTransactionFee, 
    		final Double overLimit,
    		final Double cardReplacement,
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
    	this.setForeignTransactionFee(foreignTransactionFee);
    	this.setOverLimit(overLimit);
    	this.setCardReplacement(cardReplacement);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   /**
    * @return the foreignTransactionFee
    */
   public String getForeignTransactionFee() {
	   return foreignTransactionFee;
   }

   /**
    * @param foreignTransactionFee the foreignTransactionFee to set
    */
   public void setForeignTransactionFee(String foreignTransactionFee) {
	   this.foreignTransactionFee = foreignTransactionFee;
   }
   
   /**
    * @return the overLimit
    */
   public Double getOverLimit() {
	   return overLimit;
   }

   /**
    * @param overLimit the overLimit to set
    */
   public void setOverLimit(Double overLimit) {
	   this.overLimit = overLimit;
   }
   
   /**
	 * @return the cardReplacement
	 */
   public Double getCardReplacement() {
	   return cardReplacement;
   }

   /**
	 * @param cardReplacement the cardReplacement to set
	 */
   public void setCardReplacement(Double cardReplacement) {
	   this.cardReplacement = cardReplacement;
   }

   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "OtherFee [id=" + id + ", foreignTransactionFee="
			   + foreignTransactionFee + ", overLimit=" + overLimit
			   + ", cardReplacement=" + cardReplacement + ", created=" + created
			   + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
	}
}