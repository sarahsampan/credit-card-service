package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Interest {

    private String id;
    private String creditCardId;
    private String balanceXferId;
    private String purchaseId;
    private String cashAdvanceId;
    private Double representative;
    private String minimumPayment;
    private String latePaymentCharge;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
    private BalanceXfer balanceXfer;
    private Purchase purchase;
    private CashAdvance cashAdvance;
   
    public Interest() {
        super();
    }

    public Interest(
    		final String creditCardId, 
    		final String balanceXferId,
    		final String purchaseId,
    		final String cashAdvanceId,
    		final Double representative,
    		final String minimumPayment,
    		final String latePaymentCharge,
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setCreditCardId(creditCardId);
        this.setBalanceXferId(balanceXferId);
        this.setPurchaseId(purchaseId);
        this.setCashAdvanceId(cashAdvanceId);
        this.setRepresentative(representative);
        this.setMinimumPayment(minimumPayment);
        this.setLatePaymentCharge(latePaymentCharge);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public String getCreditCardId() {
	   return creditCardId;
   }

   public void setCreditCardId(String creditCardId) {
	   this.creditCardId = creditCardId;
   }

   public String getBalanceXferId() {
	   return balanceXferId;
   }

   public void setBalanceXferId(String balanceXferId) {
	   this.balanceXferId = balanceXferId;
   }

   public String getPurchaseId() {
	   return purchaseId;
   }

   public void setPurchaseId(String purchaseId) {
	   this.purchaseId = purchaseId;
   }

   public String getCashAdvanceId() {
	   return cashAdvanceId;
   }

   public void setCashAdvanceId(String cashAdvanceId) {
	   this.cashAdvanceId = cashAdvanceId;
   }

   public Double getRepresentative() {
	   return representative;
   }

   public void setRepresentative(Double representative) {
	   this.representative = representative;
   }

   public String getMinimumPayment() {
	   return minimumPayment;
   }

   public void setMinimumPayment(String minimumPayment) {
	   this.minimumPayment = minimumPayment;
   }

   public String getLatePaymentCharge() {
	   return latePaymentCharge;
   }

   public void setLatePaymentCharge(String latePaymentCharge) {
	   this.latePaymentCharge = latePaymentCharge;
   }

   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   public BalanceXfer getBalanceXfer() {
	   return balanceXfer;
   }
	
   public void setBalanceXfer(BalanceXfer balanceXfer) {
	   this.balanceXfer = balanceXfer;
   }
	
   public Purchase getPurchase() {
	   return purchase;
   }
	  
   public CashAdvance getCashAdvance() {
	   return cashAdvance;
   }
	
   public void setCashAdvance(CashAdvance cashAdvance) {
	   this.cashAdvance = cashAdvance;
   }
	
   public void setPurchase(Purchase purchase) {
	   this.purchase = purchase;
   }

   @Override
   public String toString() {
	   return "Interest [id=" + id + ", creditCardId=" + creditCardId
			   + ", balanceXferId=" + balanceXferId + ", purchaseId=" + purchaseId
			   + ", cashAdvanceId=" + cashAdvanceId + ", representative="
			   + representative + ", minimumPayment=" + minimumPayment
			   + ", latePaymentCharge=" + latePaymentCharge + ", created="
			   + created + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
   }
}