package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class BalanceXfer {

    private String id;
    private Double rate;
    private Integer period;
    private Double fee;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public BalanceXfer() {
        super();
    }

    public BalanceXfer(final Double rate, final Integer period, final Double fee, final Timestamp created, final Timestamp modified, final String createdBy, final String modifiedBy) {
        this.setRate(rate);
        this.setPeriod(period);
        this.setFee(fee);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public Double getRate() {
	   return rate;
   }
	
   public void setRate(Double rate) {
	   this.rate = rate;
   }
	
   public Integer getPeriod() {
	   return period;
   }
	
   public void setPeriod(Integer period) {
	   this.period = period;
   }
	
   public void setFee(Double fee) {
	   this.fee = fee;
   }
	   
   public Double getFee() {
	   return fee;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "BalanceXfer [id=" + id + ", rate=" + rate + ", period=" + period
			   + ", fee=" + fee + ", created=" + created + ", modified="
			   + modified + ", createdBy=" + createdBy + ", modifiedBy="
			   + modifiedBy + "]";
   }
}