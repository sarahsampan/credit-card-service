package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Link {

    private String id;
    private String creditCardId;
    private String apply;
    private String info;
    private String provider;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public Link() {
        super();
    }

    public Link(
    		final String creditCardId, 
    		final String apply,
    		final String info,
    		final String provider,
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setCreditCardId(creditCardId);
        this.setApply(apply);
        this.setInfo(info);
        this.setProvider(provider);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public String getCreditCardId() {
	   return creditCardId;
   }

   public void setCreditCardId(String creditCardId) {
	   this.creditCardId = creditCardId;
   }

   public String getApply() {
	   return apply;
   }

   public void setApply(String apply) {
	   this.apply = apply;
   }

   public String getInfo() {
	   return info;
   }

   public void setInfo(String info) {
	   this.info = info;
   }

   public String getProvider() {
	   return provider;
   }

   public void setProvider(String provider) {
	   this.provider = provider;
   }

   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }
   
   @Override
   public String toString() {
	   return "Link [id=" + id + ", creditCardId=" + creditCardId
			   + ", apply=" + apply + ", info=" + info + ", provider="
			   + provider + ", created=" + created + ", modified=" + modified
			   + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy
			   + "]";
	}
}