package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class AnnualFee {

    private String id;
    private Double fee;
    private Double supplementaryCardFee;
    private String waiver;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public AnnualFee() {
        super();
    }

    public AnnualFee(
    		final Double fee, 
    		final Double supplementaryCardFee, 
    		final String waiver, 
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setFee(fee);
        this.setSupplementaryCardFee(supplementaryCardFee);
        this.setWaiver(waiver);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setFee(Double fee) {
	   this.fee = fee;
   }
	   
   public Double getFee() {
	   return fee;
   }
   
   public void setSupplementaryCardFee(Double supplementaryCardFee) {
	   this.supplementaryCardFee = supplementaryCardFee;
   }
   
   public Double getSupplementaryCardFee() {
	   return supplementaryCardFee;
   }

   public void setWaiver(String waiver) {
	   this.waiver = waiver;
   }
	   
   public String getWaiver() {
	   return waiver;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "AnnualFee [id=" + id + ", fee=" + fee + ", supplementaryCardFee="
			   + supplementaryCardFee + ", waiver=" + waiver + ", created="
			   + created + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
	}
}