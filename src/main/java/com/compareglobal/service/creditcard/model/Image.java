package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Image {

    private String id;
    private String creditCardId;
    private String icon;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public Image() {
        super();
    }

    public Image(
    		final String creditCardId,
    		final String icon,
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
    	this.setCreditCardId(creditCardId);
        this.setIcon(icon);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setCreditCardId(String creditCardId) {
       this.creditCardId = creditCardId;
   }
	   
   public String getCreditCardId() {
       return creditCardId;
   }
   
   public void setIcon(String icon) {
	   this.icon = icon;
   }
	   
   public String getIcon() {
	   return icon;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "Image [id=" + id + ", icon=" + icon + ", created=" + created
			   + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
   }
}