package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class CreditCard extends ResourceSupport{
	
    private String id;
    private String brandId;
    private String name;
    private String tags;
    private String language;
    private Integer active;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
    private Interest interest;
    private Link link;
    private Benefit benefit;
    private Fee fee;
    private Reward reward;
    private Eligibility eligibility;
    private Form form;
    
   
    public CreditCard() {
        super();
    }
    
    @JsonCreator
    public CreditCard(
    		@JsonProperty("id") final String id,
    		@JsonProperty("brandId") final String brandId, 
    		@JsonProperty("name") final String name, 
    		@JsonProperty("tags") final String tags, 
    		@JsonProperty("language") final String language, 
    		@JsonProperty("active") final Integer active, 
    		@JsonProperty("created") final Timestamp created, 
    		@JsonProperty("modified") final Timestamp modified, 
    		@JsonProperty("createdBy") final String createdBy, 
    		@JsonProperty("modifiedBy") final String modifiedBy
    		) {
    	this.setCreditCardId(id);
        this.setBrandId(brandId);
        this.setName(name);
        this.setTags(tags);
        this.setLanguage(language);
        this.setActive(active);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setCreditCardId(String id) {
       this.id = id;
   }
	   
   public String getCreditCardId() {
       return id;
   }
   
   public void setBrandId(String brandId) {
      this.brandId = brandId;
   }
	   
   public String getBrandId() {
      return brandId;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   public String getName() {
      return name;
   }

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getCreated() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(created);
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public String getModified() {
		Date dt = new Date(modified.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(dt);
	}

	public void setModified(Timestamp modified) {
		this.modified = modified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Interest getInterest() {
		return interest;
	}

	public void setInterest(Interest interest) {
		this.interest = interest;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public Benefit getBenefit() {
		return benefit;
	}

	public void setBenefit(Benefit benefit) {
		this.benefit = benefit;
	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}

	public Eligibility getEligibility() {
		return eligibility;
	}

	public void setEligibility(Eligibility eligibility) {
		this.eligibility = eligibility;
	}
	
	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

}