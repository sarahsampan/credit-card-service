package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Eligibility {

    private String id;
    private String creditCardId;
    private Integer ageMin;
    private Integer ageMax;
    private Integer supplementaryCard;
    private Integer income;
    private Boolean existingCardHolder;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public Eligibility() {
        super();
    }

    public Eligibility(
    		final String creditCardId, 
    		final Integer ageMin, 
    		final Integer ageMax, 
    		final Integer supplementaryCard, 
    		final Integer income, 
    		final Boolean existingCardHolder, 
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setCreditCardId(creditCardId);
        this.setAgeMin(ageMin);
        this.setAgeMax(ageMax);
        this.setSupplementaryCard(supplementaryCard);
        this.setIncome(income);
        this.setExistingCardHolder(existingCardHolder);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setCreditCardId(String creditCardId) {
	   this.creditCardId = creditCardId;
   }
	   
   public String getCreditCardId() {
	   return creditCardId;
   }
   
   public void setAgeMin(Integer ageMin) {
	   this.ageMin = ageMin;
   }
   
   public Integer getAgeMin() {
	   return ageMin;
   }
   
   public void setAgeMax(Integer ageMax) {
	   this.ageMax = ageMax;
   }
   
   public Integer getAgeMax() {
	   return ageMax;
   }
   
   public void setSupplementaryCard(Integer supplementaryCard) {
	   this.supplementaryCard = supplementaryCard;
   }
   
   public Integer getSupplementaryCard() {
	   return supplementaryCard;
   }
   
   public void setIncome(Integer income) {
	   this.income = income;
   }
   
   public Integer getIncome() {
	   return income;
   }
   
   public void setExistingCardHolder(Boolean existingCardHolder) {
	   this.existingCardHolder = existingCardHolder;
   }
   
   public Boolean getExistingCardHolder() {
	   return existingCardHolder;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "Eligibility [id=" + id + ", creditCardId=" + creditCardId
			   + ", ageMin=" + ageMin + ", ageMax=" + ageMax
			   + ", supplementaryCard=" + supplementaryCard + ", income=" + income
			   + ", existingCardHolder=" + existingCardHolder + ", created="
			   + created + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
   }
}