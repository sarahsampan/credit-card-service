package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class CashAdvance {

    private String id;
    private Double rate;
    private Double fee;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public CashAdvance() {
        super();
    }

    public CashAdvance(
    		final Double rate, 
    		final Double fee, 
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setRate(rate);
        this.setFee(fee);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setRate(Double rate) {
	   this.rate = rate;
   }
	   
   public Double getRate() {
	   return rate;
   }
   
   public void setFee(Double fee) {
	   this.fee = fee;
   }
   
   public Double getFee() {
	   return fee;
   }
   
   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "CashAdvance [id=" + id + ", rate=" + rate + ", fee=" + fee
			   + ", created=" + created + ", modified=" + modified
			   + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy + "]";
   }
}