package com.compareglobal.service.creditcard.model;



public enum FilterCode {
	    CASHBACK("cashback"),
	    POPULAR("popular"),
	    LOW_APR("low-apr"),
	    REWARD("reward"),
	    BAD_CREDIT("bad-credit"),
	    TRAVEL("travel"),
	    CREDIT_BUILDING("credit-building"),
	    ZERO_PERCENT_TRAVEL("zero-percent-travel"),
	    ZERO_PERCENT_PURCHASE("zero-percent-purchase");
	
	    private String statusCode;
	    
		private FilterCode(String s) {
			statusCode = s;
		}
	 
		public String getStatusCode() {
			return statusCode;
		}
}

