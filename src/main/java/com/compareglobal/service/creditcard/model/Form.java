package com.compareglobal.service.creditcard.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class Form {

    private String key;
    private String type;
    private String width;
    private String height;
    private String placeholder;
    private String fieldHtmlClass;
    private String htmlClass;

    public Form() {
    }
    
    @JsonCreator
    public Form(
    		@JsonProperty("key") String key, 
    		@JsonProperty("type") String type, 
    		@JsonProperty("width") String width, 
    		@JsonProperty("height") String height, 
    		@JsonProperty("placeholder") String placeholder, 
    		@JsonProperty("fieldHtmlClass") String fieldHtmlClass, 
    		@JsonProperty("htmlClass") String htmlClass) {
    	this.setKey(key);
    	this.setType(type);
    	this.setWidth(width);
    	this.setHeight(height);
    	this.setPlaceholder(placeholder);
    	this.setFieldHtmlClass(fieldHtmlClass);
    	this.setHtmlClass(htmlClass);
    }

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return the placeholder
	 */
	public String getPlaceholder() {
		return placeholder;
	}

	/**
	 * @param placeholder the placeholder to set
	 */
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	/**
	 * @return the fieldHtmlClass
	 */
	public String getFieldHtmlClass() {
		return fieldHtmlClass;
	}

	/**
	 * @param fieldHtmlClass the fieldHtmlClass to set
	 */
	public void setFieldHtmlClass(String fieldHtmlClass) {
		this.fieldHtmlClass = fieldHtmlClass;
	}

	/**
	 * @return the htmlClass
	 */
	public String getHtmlClass() {
		return htmlClass;
	}

	/**
	 * @param htmlClass the htmlClass to set
	 */
	public void setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
	}

	@Override
	public String toString() {
		return "Form [key=" + key + ", type=" + type + ", width=" + width
				+ ", height=" + height + ", placeholder=" + placeholder
				+ ", fieldHtmlClass=" + fieldHtmlClass + ", htmlClass="
				+ htmlClass + "]";
	}
}
