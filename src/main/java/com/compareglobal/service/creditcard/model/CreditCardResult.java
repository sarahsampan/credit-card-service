package com.compareglobal.service.creditcard.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class CreditCardResult {

    private Form form;

    public CreditCardResult() {
    }
    
    @JsonCreator
    public CreditCardResult(@JsonProperty("form") Form form) {
    	this.setForm(form);
    }

	/**
	 * @return the form
	 */
	public Form getForm() {
		return form;
	}

	/**
	 * @param form the form to set
	 */
	public void setForm(Form form) {
		this.form = form;
	}
}
