package com.compareglobal.service.creditcard.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class AirMile {

    private String id;
    private String conditionId;
    private String conversion;
    private Timestamp created;
    private Timestamp modified;
    private String createdBy;
    private String modifiedBy;
   
    public AirMile() {
        super();
    }

    public AirMile(
    		final String conditionId, 
    		final String conversion, 
    		final Timestamp created, 
    		final Timestamp modified, 
    		final String createdBy, 
    		final String modifiedBy
    		) {
        this.setConditionId(conditionId);
        this.setConversion(conversion);
        this.setCreated(created);
        this.setModified(modified);
        this.setCreatedBy(createdBy);
        this.setModifiedBy(modifiedBy);
   }
   
   public void setId(String id) {
       this.id = id;
   }
	   
   public String getId() {
       return id;
   }
   
   public void setConditionId(String conditionId) {
	   this.conditionId = conditionId;
   }
	   
   public String getConditionId() {
	   return conditionId;
   }
   
   public void setConversion(String conversion) {
	   this.conversion = conversion;
   }
   
   public String getConversion() {
	   return conversion;
   }

   public String getCreated() {
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(created);
   }

   public void setCreated(Timestamp created) {
	   this.created = created;
   }

   public String getModified() {
	   Date dt = new Date(modified.getTime());
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   return sdf.format(dt);
   }

   public void setModified(Timestamp modified) {
	   this.modified = modified;
   }

   public String getCreatedBy() {
	   return createdBy;
   }

   public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
   }

   public String getModifiedBy() {
	   return modifiedBy;
   }

   public void setModifiedBy(String modifiedBy) {
	   this.modifiedBy = modifiedBy;
   }

   @Override
   public String toString() {
	   return "AirMile [id=" + id + ", conditionId=" + conditionId
			   + ", conversion=" + conversion + ", created=" + created
			   + ", modified=" + modified + ", createdBy=" + createdBy
			   + ", modifiedBy=" + modifiedBy + "]";
   }
}