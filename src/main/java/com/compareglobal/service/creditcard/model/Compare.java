package com.compareglobal.service.creditcard.model;


public class Compare {
	
	private String key;
	private String query;
	private String language;
	
	public Compare() {
	}

	public void findCompareQueryString() {	
		
		query = "SELECT * FROM credit_cards"; 
		
		if(language != null ) {
			query = query +  " WHERE language ='"+ language +"'";
		}
		if (key != null) {
			switch(FilterCode.valueOf(key.toUpperCase())) {
		        case CASHBACK : { 
		        	query = query + " AND name LIKE '%Cash%Back%'";
		        	break;
		        }
		        case POPULAR  : {
		        	query = query + " AND NAME LIKE '%visa'";       
		        	break;
		        }
				default:
					break;       
			}
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
