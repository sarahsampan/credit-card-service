package com.compareglobal.service.creditcard.util;

/**
 * Provides some constants and utility methods to build a Link Header to be stored in the {@link javax.servlet.http.HttpServletResponse} object
 */
public final class CreditCardUtil {

    private CreditCardUtil() {
        throw new AssertionError();
    }

    public static String getLocale(final String locale) {
    	String[] split = locale.split("-");
        return split[0];
    }
}
