package com.compareglobal.service.creditcard;

import com.compareglobal.service.creditcard.drools.config.DroolsConfig;
import com.mangofactory.swagger.plugin.EnableSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@EnableSwagger
@Import({DroolsConfig.class})
public class CreditCardApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CreditCardApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CreditCardApplication.class, args);
    }
}