package com.compareglobal.service.creditcard.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.compareglobal.service.creditcard.dao.CreditCardDAO;
import com.compareglobal.service.creditcard.model.Compare;
import com.compareglobal.service.creditcard.model.CreditCard;

@Service
public class CreditCardService {
	
	private final KieBase kbase;
	private ApplicationContext context;
	
	static final Logger logger = Logger.getLogger(CreditCardService.class);

    @Inject
    public CreditCardService(KieBase kbase) {
    	this.kbase = kbase;
    }

    @Metered
    @ExceptionMetered
    public List<CreditCard> check(String locale, String filter ) {
    	String[] split = locale.split("-");
    	String language = split[0];
    	
    	List<CreditCard> creditCardArray = new ArrayList<CreditCard>();
    	String queryFilter = "";
    	
    	
    	context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//apply to rules
    	KieSession ksession = kbase.newKieSession();
    	List list = new ArrayList();
		ksession.setGlobal("list", list);
		
		try {
			Compare comp = new Compare();
			comp.setLanguage(language);
			ksession.insert(filter.toUpperCase());
			ksession.insert(comp);
				
			// Fire all rules
			ksession.fireAllRules();
	
			for (int i=0; i< list.size(); i++){
			    queryFilter = (String) list.get(i);
			}
			       
			//search to DB
			CreditCardDAO creditCardDAO = (CreditCardDAO)context.getBean("creditCardDAO");
			creditCardArray = creditCardDAO.compare(queryFilter);
		        
	    } catch (Exception e) {
			logger.error("ERROR in CreditCardService >>>");
			e.printStackTrace();
		}
		finally {
			//destroy session.
			ksession.dispose();
		}
		
        
        // Return result
        return creditCardArray;
    }
}