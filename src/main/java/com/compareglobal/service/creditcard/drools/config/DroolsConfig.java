package com.compareglobal.service.creditcard.drools.config;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.*;
import org.kie.api.builder.Message.Level;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;

/**
 * Created by Stephan on 17-06-14.
 */
@Configuration
@ComponentScan(basePackages = {"com.compareglobal.service.creditcard"})
public class DroolsConfig {

	static final Logger logger = Logger.getLogger(DroolsConfig.class);
	
    private Resource[] listRules() throws IOException {
        PathMatchingResourcePatternResolver pmrs = new PathMatchingResourcePatternResolver();
        Resource[] resources = pmrs.getResources("classpath*:com/compareglobal/service/creditcard/drools/rules/*.drl");
        return resources;
    }

    @Bean
    public KieContainer kieContainer() throws IOException {
        KieServices ks = KieServices.Factory.get();
        final KieRepository kr = ks.getRepository();
        kr.addKieModule(new KieModule() {
            @Override
            public ReleaseId getReleaseId() {
                return kr.getDefaultReleaseId();
            }
        });
        KieFileSystem kfs = ks.newKieFileSystem();
        Resource[] files = listRules();

        for(Resource file : files) {
            String myString = IOUtils.toString(file.getInputStream(), "UTF-8");
            kfs.write("src/main/resources/"+ file.getFilename(), myString);
        }

        KieBuilder kb = ks.newKieBuilder(kfs);
        kb.buildAll(); // kieModule is automatically deployed to KieRepository if successfully built.
        
        if (kb.getResults().hasMessages(Level.ERROR)) 
        {
        	logger.error("Drools Build Errors:\n" + kb.getResults().toString());
            throw new RuntimeException("Drools Build Errors:\n" + kb.getResults().toString());
        }
        
        KieContainer kContainer = ks.newKieContainer(kr.getDefaultReleaseId());
        return kContainer;
    }

    @Bean
    public KieBase kieBase() throws IOException {
        KieBase kieBase = kieContainer().getKieBase();
        return kieBase;
    }
}