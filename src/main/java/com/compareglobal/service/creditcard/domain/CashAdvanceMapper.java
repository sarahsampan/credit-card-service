package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.CashAdvance;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CashAdvanceMapper implements RowMapper<CashAdvance> {
    public CashAdvance mapRow(ResultSet rs, int rowNum) throws SQLException {
    	CashAdvance cashAdvance = new CashAdvance();
    	cashAdvance.setId(rs.getString("id"));
    	cashAdvance.setRate(rs.getDouble("rate"));
    	cashAdvance.setFee(rs.getDouble("fee"));
    	cashAdvance.setCreated(rs.getTimestamp("created"));
    	cashAdvance.setModified(rs.getTimestamp("modified"));
    	cashAdvance.setCreatedBy(rs.getString("created_by"));
    	cashAdvance.setModifiedBy(rs.getString("modified_by"));
        return cashAdvance;
    }
}