package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.BalanceXfer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BalanceXferMapper implements RowMapper<BalanceXfer> {
    public BalanceXfer mapRow(ResultSet rs, int rowNum) throws SQLException {
    	BalanceXfer balanceXfer = new BalanceXfer();
    	balanceXfer.setId(rs.getString("id"));
    	balanceXfer.setRate(rs.getDouble("rate"));
    	balanceXfer.setPeriod(rs.getInt("period"));
    	balanceXfer.setFee(rs.getDouble("fee"));
    	balanceXfer.setCreated(rs.getTimestamp("created"));
    	balanceXfer.setModified(rs.getTimestamp("modified"));
    	balanceXfer.setCreatedBy(rs.getString("created_by"));
    	balanceXfer.setModifiedBy(rs.getString("modified_by"));
        return balanceXfer;
    }
}