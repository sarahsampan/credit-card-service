package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Benefit;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BenefitMapper implements RowMapper<Benefit> {
    public Benefit mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Benefit benefit = new Benefit();
    	benefit.setId(rs.getString("id"));
    	benefit.setCreditCardId(rs.getString("credit_card_id"));
    	benefit.setDescription(rs.getString("description"));
    	benefit.setCreated(rs.getTimestamp("created"));
    	benefit.setModified(rs.getTimestamp("modified"));
    	benefit.setCreatedBy(rs.getString("created_by"));
    	benefit.setModifiedBy(rs.getString("modified_by"));
        return benefit;
    }
}