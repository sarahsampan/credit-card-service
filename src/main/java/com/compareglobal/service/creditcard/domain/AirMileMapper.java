package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.AirMile;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AirMileMapper implements RowMapper<AirMile> {
    public AirMile mapRow(ResultSet rs, int rowNum) throws SQLException {
        AirMile airMile = new AirMile();
        airMile.setId(rs.getString("id"));
        airMile.setConditionId(rs.getString("condition_id"));
        airMile.setConversion(rs.getString("conversion"));
        airMile.setCreated(rs.getTimestamp("created"));
        airMile.setModified(rs.getTimestamp("modified"));
        airMile.setCreatedBy(rs.getString("created_by"));
        airMile.setModifiedBy(rs.getString("modified_by"));
        return airMile;
    }
}