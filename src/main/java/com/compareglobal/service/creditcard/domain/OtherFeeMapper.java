package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.OtherFee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OtherFeeMapper implements RowMapper<OtherFee> {
    public OtherFee mapRow(ResultSet rs, int rowNum) throws SQLException {
    	OtherFee otherFee = new OtherFee();
    	otherFee.setId(rs.getString("id"));
    	otherFee.setForeignTransactionFee(rs.getString("foreign_transaction_fee"));
    	otherFee.setOverLimit(rs.getDouble("over_limit"));
    	otherFee.setCardReplacement(rs.getDouble("card_replacement"));
    	otherFee.setCreated(rs.getTimestamp("created"));
    	otherFee.setModified(rs.getTimestamp("modified"));
    	otherFee.setCreatedBy(rs.getString("created_by"));
    	otherFee.setModifiedBy(rs.getString("modified_by"));
        return otherFee;
    }
}