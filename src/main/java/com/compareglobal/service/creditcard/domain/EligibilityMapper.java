package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Eligibility;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EligibilityMapper implements RowMapper<Eligibility> {
    public Eligibility mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Eligibility eligibility = new Eligibility();
    	eligibility.setId(rs.getString("id"));
    	eligibility.setAgeMin(rs.getInt("age_min"));
    	eligibility.setAgeMax(rs.getInt("rate"));
    	eligibility.setSupplementaryCard(rs.getInt("supplementary_card"));
    	eligibility.setIncome(rs.getInt("income"));
    	eligibility.setExistingCardHolder(rs.getBoolean("existing_card_holder"));
    	eligibility.setCreated(rs.getTimestamp("created"));
    	eligibility.setModified(rs.getTimestamp("modified"));
    	eligibility.setCreatedBy(rs.getString("created_by"));
    	eligibility.setModifiedBy(rs.getString("modified_by"));
        return eligibility;
    }
}