package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Interest;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InterestMapper implements RowMapper<Interest> {
    public Interest mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Interest interest = new Interest();
    	interest.setId(rs.getString("id"));
    	interest.setCreditCardId(rs.getString("credit_card_id"));
    	interest.setBalanceXferId(rs.getString("balance_xfer_id"));
    	interest.setPurchaseId(rs.getString("purchase_id"));
    	interest.setCashAdvanceId(rs.getString("cash_advance_id"));
    	interest.setRepresentative(rs.getDouble("representative"));
    	interest.setMinimumPayment(rs.getString("minimum_payment"));
    	interest.setLatePaymentCharge(rs.getString("late_payment_charge"));
    	interest.setCreated(rs.getTimestamp("created"));
    	interest.setModified(rs.getTimestamp("modified"));
    	interest.setCreatedBy(rs.getString("created_by"));
    	interest.setModifiedBy(rs.getString("modified_by"));
        return interest;
    }
}