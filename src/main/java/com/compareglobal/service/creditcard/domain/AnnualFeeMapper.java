package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.AnnualFee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnnualFeeMapper implements RowMapper<AnnualFee> {
    public AnnualFee mapRow(ResultSet rs, int rowNum) throws SQLException {
    	AnnualFee annualFee = new AnnualFee();
    	annualFee.setId(rs.getString("id"));
    	annualFee.setSupplementaryCardFee(rs.getDouble("supplementary_card_fee"));
    	annualFee.setWaiver(rs.getString("waiver"));
    	annualFee.setCreated(rs.getTimestamp("created"));
    	annualFee.setModified(rs.getTimestamp("modified"));
    	annualFee.setCreatedBy(rs.getString("created_by"));
    	annualFee.setModifiedBy(rs.getString("modified_by"));
        return annualFee;
    }
}