package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.CreditCard;
import org.springframework.stereotype.Repository;

@Repository
public class CreditCardRepository {

    // Stuff that we will retrieve from the database.
    private static CreditCard creditCard = new CreditCard();

    public CreditCard getCreditCard() {
        return creditCard;
    }
}
