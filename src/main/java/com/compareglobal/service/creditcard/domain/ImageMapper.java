package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Image;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ImageMapper implements RowMapper<Image> {
    public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Image image = new Image();
    	image.setId(rs.getString("id"));
    	image.setCreditCardId(rs.getString("credit_card_id"));
    	image.setIcon(rs.getString("icon"));
    	image.setCreated(rs.getTimestamp("created"));
    	image.setModified(rs.getTimestamp("modified"));
    	image.setCreatedBy(rs.getString("created_by"));
    	image.setModifiedBy(rs.getString("modified_by"));
        return image;
    }
}