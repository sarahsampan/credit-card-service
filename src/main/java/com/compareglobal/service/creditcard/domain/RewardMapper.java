package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Reward;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RewardMapper implements RowMapper<Reward> {
    public Reward mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Reward reward = new Reward();
    	reward.setId(rs.getString("id"));
    	reward.setCreditCardId(rs.getString("credit_card_id"));
    	reward.setAirmileId(rs.getString("airmile_id"));
    	reward.setShopping(rs.getString("shopping"));
    	reward.setEntertainment(rs.getString("entertainment"));
    	reward.setCreated(rs.getTimestamp("created"));
    	reward.setModified(rs.getTimestamp("modified"));
    	reward.setCreatedBy(rs.getString("created_by"));
    	reward.setModifiedBy(rs.getString("modified_by"));
        return reward;
    }
}