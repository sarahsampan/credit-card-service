package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Link;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LinkMapper implements RowMapper<Link> {
    public Link mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Link link = new Link();
    	link.setId(rs.getString("id"));
    	link.setCreditCardId(rs.getString("credit_card_id"));
    	link.setApply(rs.getString("apply"));
    	link.setInfo(rs.getString("info"));
    	link.setProvider(rs.getString("provider"));
    	link.setCreated(rs.getTimestamp("created"));
    	link.setModified(rs.getTimestamp("modified"));
    	link.setCreatedBy(rs.getString("created_by"));
    	link.setModifiedBy(rs.getString("modified_by"));
        return link;
    }
}