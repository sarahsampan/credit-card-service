package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Address;
import com.compareglobal.service.creditcard.model.Blacklist;
import com.compareglobal.service.creditcard.model.RuleParam;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class FraudRepository {

    // Stuff that we will retrieve from the database. Here now for testing.
    private static RuleParam ruleParam = new RuleParam();
    private static Blacklist blacklist = new Blacklist();

    static {
        blacklist.addBlacklist(Blacklist.ADDRESSES, new ArrayList() {{ add(new Address("3452RK", 53)); }});
    }

    public RuleParam getRuleParam() {
        return ruleParam;
    }

    public Blacklist getBlacklist() {
        return blacklist;
    }
}
