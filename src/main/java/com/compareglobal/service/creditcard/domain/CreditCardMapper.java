package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.CreditCard;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditCardMapper implements RowMapper<CreditCard> {
   public CreditCard mapRow(ResultSet rs, int rowNum) throws SQLException {
      CreditCard creditCard = new CreditCard();
      creditCard.setCreditCardId(rs.getString("id"));
      creditCard.setBrandId(rs.getString("brand_id"));
      creditCard.setName(rs.getString("name"));
      creditCard.setLanguage(rs.getString("language"));
      creditCard.setTags(rs.getString("tags"));
      creditCard.setActive(rs.getInt("active"));
      creditCard.setCreated(rs.getTimestamp("created"));
      creditCard.setModified(rs.getTimestamp("modified"));
      creditCard.setCreatedBy(rs.getString("created_by"));
      creditCard.setModifiedBy(rs.getString("modified_by"));
      return creditCard;
   }
}