package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Fee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeeMapper implements RowMapper<Fee> {
    public Fee mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Fee fee = new Fee();
    	fee.setId(rs.getString("id"));
    	fee.setCreditCardId(rs.getString("credit_card_id"));
    	fee.setAnnualFeeId(rs.getString("annual_fee_id"));
    	fee.setOtherFeeId(rs.getString("other_fee_id"));
    	fee.setCreated(rs.getTimestamp("created"));
    	fee.setModified(rs.getTimestamp("modified"));
    	fee.setCreatedBy(rs.getString("created_by"));
    	fee.setModifiedBy(rs.getString("modified_by"));
        return fee;
    }
}