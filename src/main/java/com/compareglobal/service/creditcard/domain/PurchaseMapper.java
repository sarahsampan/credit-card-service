package com.compareglobal.service.creditcard.domain;

import com.compareglobal.service.creditcard.model.Purchase;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PurchaseMapper implements RowMapper<Purchase> {
    public Purchase mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Purchase purchase = new Purchase();
    	purchase.setId(rs.getString("id"));
    	purchase.setRate(rs.getDouble("rate"));
    	purchase.setPeriod(rs.getInt("period"));
    	purchase.setCreated(rs.getTimestamp("created"));
    	purchase.setModified(rs.getTimestamp("modified"));
    	purchase.setCreatedBy(rs.getString("created_by"));
    	purchase.setModifiedBy(rs.getString("modified_by"));
        return purchase;
    }
}