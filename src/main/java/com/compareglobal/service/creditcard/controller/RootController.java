package com.compareglobal.service.creditcard.controller;


import com.compareglobal.service.creditcard.util.LinkUtil;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

@Controller
public class RootController {

    public RootController() {
        super();
    }
    
    @RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void root(final HttpServletRequest request, final HttpServletResponse response) {
		final String rootUri = request.getRequestURL().toString();

		final URI fooUri = new UriTemplate("{rootUri}").expand(
				rootUri);

		final String linkToFoo = LinkUtil.createLinkHeader(
                fooUri.toASCIIString(), "collection");
		
		response.addHeader("Link", linkToFoo);
	}
}
