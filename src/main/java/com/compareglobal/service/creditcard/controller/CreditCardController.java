package com.compareglobal.service.creditcard.controller;

import com.compareglobal.service.creditcard.model.CreditCard;
import com.compareglobal.service.creditcard.service.CreditCardService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;


@Api(value = "compare", description = "Credit card service API") // Swagger annotation
@RestController
@RequestMapping("/compare")
public class CreditCardController {	
    
	@Autowired
    private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private CreditCardService creditCardService;
    
	@Inject
	public CreditCardController(CreditCardService creditCardService) {
		this.creditCardService = creditCardService;
	}
	
	@ApiOperation("filter")
	@RequestMapping(params = {"locale", "filter"} , method = RequestMethod.GET )
	@ResponseBody public List<CreditCard> compare( @RequestParam("locale") String locale, @RequestParam("filter") String filter) {
		return creditCardService.check(locale, filter);		
    }
}