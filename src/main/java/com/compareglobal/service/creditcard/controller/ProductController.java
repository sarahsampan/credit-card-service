package com.compareglobal.service.creditcard.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.compareglobal.service.creditcard.dao.CreditCardDAO;
import com.compareglobal.service.creditcard.model.CreditCard;
import com.compareglobal.service.creditcard.model.CreditCardResult;
import com.compareglobal.service.creditcard.model.Form;
import com.compareglobal.service.creditcard.model.Greeting;
import com.compareglobal.service.creditcard.service.CreditCardService;
import com.compareglobal.service.creditcard.util.CreditCardUtil;
import com.compareglobal.service.creditcard.util.RestPreconditions;

import com.google.common.base.Preconditions;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

@Api(value = "Product Controller", description = "Product service API") // Swagger annotation
@RestController
@RequestMapping("/products")
public class ProductController {	
    
	static final Logger logger = Logger.getLogger(ProductController.class);
	
	@Autowired
    private ApplicationEventPublisher eventPublisher;
	
	private CreditCardService creditCardService;
	private ApplicationContext context;
	private CreditCardDAO creditCardDAO;
    
	@Inject
	public ProductController(CreditCardService creditCardService) {
		this.creditCardService = creditCardService;
    	context = new ClassPathXmlApplicationContext("applicationContext.xml");
    	creditCardDAO = (CreditCardDAO)context.getBean("creditCardDAO");
	}
	
    @ApiOperation("getOne")
    @RequestMapping( params = {"locale"}, value = "/{id}", method = RequestMethod.GET )
    @ResponseBody public ResponseEntity<List<CreditCard>> getOne(@RequestParam("locale") final String locale, @PathVariable("id") final String id){
    	final String language = CreditCardUtil.getLocale(locale);
        List<CreditCard> creditCardArray =  creditCardDAO.findOne(language, id);
        
        for (CreditCard creditCard : creditCardArray){
        	creditCard.add(linkTo(methodOn(ProductController.class).getOne(language, id)).withSelfRel());
        }
        
        return new ResponseEntity<List<CreditCard>>(creditCardArray, HttpStatus.OK);
        
    }
    
    @ApiOperation("get")
    @RequestMapping( params = {"locale"}, method = RequestMethod.GET )
    @ResponseBody public ResponseEntity<List<CreditCard>> get(@RequestParam("locale") final String locale){
    	String language = CreditCardUtil.getLocale(locale);
        List<CreditCard> creditCardArray =  creditCardDAO.findAll(language);
        
        for (CreditCard creditCard : creditCardArray){
        	creditCard.add(linkTo(methodOn(ProductController.class).getOne(language, creditCard.getCreditCardId())).withSelfRel());
        }
        
        return new ResponseEntity<List<CreditCard>>(creditCardArray, HttpStatus.OK);
    }
    
    @ApiOperation("post")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void post(@RequestBody final CreditCard resource) {
        Preconditions.checkNotNull(resource);
        creditCardDAO.create(resource);
    }

    @ApiOperation("put")
    @RequestMapping(params = {"locale"}, value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("locale") final String locale, @PathVariable("id") final String id, @RequestBody final CreditCard resource) {
    	final String language = CreditCardUtil.getLocale(locale);
    	Preconditions.checkNotNull(resource);
    	RestPreconditions.checkFound(creditCardDAO.findOne(language, id));
    	creditCardDAO.update(resource, id);
    }
    
    @ApiOperation("delete")
    @RequestMapping(params = {"locale"}, value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") final String id) {
        creditCardDAO.deleteById(id);
    }
    
    @ApiOperation("schema")
    @RequestMapping(value = "/schema", method = RequestMethod.GET)
    @ResponseBody public JsonSchema getSchema() throws JsonMappingException {
        ObjectMapper m = new ObjectMapper();
        SchemaFactoryWrapper visitor = new SchemaFactoryWrapper();
        
        m.acceptJsonFormatVisitor(m.constructType(CreditCard.class), visitor);
        
        JsonSchema jsonSchema = visitor.finalSchema();

        return jsonSchema;
    }
}