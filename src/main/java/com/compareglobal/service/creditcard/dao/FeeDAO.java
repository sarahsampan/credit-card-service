package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Fee;

import javax.sql.DataSource;
import java.util.List;

public interface FeeDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Fees table corresponding
    * to a passed fee id.
    */
   List<Fee> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Fees table.
    */
   List<Fee> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Fees table.
    */
   void create(Fee fee);
   
   /** 
    * This is the method to be used to create
    * a record in the Fees table.
    */
   void update(Fee fee, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Fees table corresponding
    * to a passed fee id.
    */
   void deleteById(String id);
}
