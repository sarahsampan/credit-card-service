package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.CreditCard;

import javax.sql.DataSource;
import java.util.List;

public interface CreditCardDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Credit Cards table corresponding
    * to a passed credit card id.
    */
   List<CreditCard> findOne(String language, String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Credit Cards table.
    */
   List<CreditCard> findAll(String language);

   /** 
    * This is the method to be used to create
    * a record in the Credit Cards table.
    */
   void create(CreditCard creditCard);
   
   /** 
    * This is the method to be used to create
    * a record in the Credit Cards table.
    */
   void update(CreditCard creditCard, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Credit Cards table corresponding
    * to a passed credit card id.
    */
   void deleteById(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Credit Cards table.
    */
   public List<CreditCard> compare(String query);
   
}
