package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.AirMile;

import javax.sql.DataSource;
import java.util.List;

public interface AirMileDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Air Miles table corresponding
    * to a passed air mile id.
    */
   List<AirMile> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Air Miles table.
    */
   List<AirMile> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Air Miles table.
    */
   void create(AirMile airMile);
   
   /** 
    * This is the method to be used to create
    * a record in the Air Miles table.
    */
   void update(AirMile airMile, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Air Miles table corresponding
    * to a passed air mile id.
    */
   void deleteById(String id);
}
