package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Eligibility;

import javax.sql.DataSource;
import java.util.List;

public interface EligibilityDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Eligibilities table corresponding
    * to a passed eligibility id.
    */
   List<Eligibility> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Eligibilities table.
    */
   List<Eligibility> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Eligibilities table.
    */
   void create(Eligibility eligibility);
   
   /** 
    * This is the method to be used to create
    * a record in the Eligibilities table.
    */
   void update(Eligibility eligibility, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Eligibilities table corresponding
    * to a passed eligibility id.
    */
   void deleteById(String id);
}
