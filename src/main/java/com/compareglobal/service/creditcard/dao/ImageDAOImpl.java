package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.ImageMapper;
import com.compareglobal.service.creditcard.model.Image;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;

import java.text.SimpleDateFormat;
import java.util.*;

public class ImageDAOImpl implements ImageDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<Image> findOne(String id) {
      String SQL = "SELECT * FROM images WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<Image> image = (List<Image>) namedParameterJdbcTemplate.query(SQL, namedParameters, new ImageMapper());
      return image;
   }
   
   @Override 
   public List<Image> findAll() {
	  String SQL = "SELECT * FROM images";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<Image> image = (List<Image>) namedParameterJdbcTemplate.query(SQL, namedParameters, new ImageMapper());
      return image;
   }
   
   @Override 
   public void update(final Image image, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((image.getCreditCardId() != null) && !(image.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", image.getCreditCardId());
       }
       
       if ((image.getIcon() != null) && !(image.getIcon().equals(""))) {
    	   updateParams = "icon = :icon, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("icon", image.getIcon()); 
       }
       
       if ((image.getModifiedBy() != null) && !(image.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", image.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE images SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Image image) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO images ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "icon, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :icon, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", image.getCreditCardId());
      namedParameters.put("icon", image.getIcon());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", image.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM images WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}