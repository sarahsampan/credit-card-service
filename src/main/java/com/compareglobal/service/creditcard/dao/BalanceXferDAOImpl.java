package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.BalanceXferMapper;
import com.compareglobal.service.creditcard.model.BalanceXfer;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class BalanceXferDAOImpl implements BalanceXferDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<BalanceXfer> findOne(String id) {
      String SQL = "SELECT * FROM balance_xfers WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<BalanceXfer> balanceXfer = (List<BalanceXfer>) namedParameterJdbcTemplate.query(SQL, namedParameters, new BalanceXferMapper());
      return balanceXfer;
   }
   
   @Override 
   public List<BalanceXfer> findAll() {
	  String SQL = "SELECT * FROM balance_xfers";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<BalanceXfer> balanceXfer = (List<BalanceXfer>) namedParameterJdbcTemplate.query(SQL, namedParameters, new BalanceXferMapper());
      return balanceXfer;
   }
   
   @Override 
   public void update(final BalanceXfer balanceXfer, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((balanceXfer.getRate() != null) && !(balanceXfer.getRate().equals(""))) {
    	   updateParams = "rate = :rate, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("rate", balanceXfer.getRate()); 
       }
       
       if ((balanceXfer.getPeriod() != null) && !(balanceXfer.getPeriod().equals(""))) {
    	   updateParams = "period = :period, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("period", balanceXfer.getPeriod());
       }
       
       if ((balanceXfer.getFee() != null) && !(balanceXfer.getFee().equals(""))) {
    	   updateParams = "fee = :fee, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("fee", balanceXfer.getFee());
       }
       
       if ((balanceXfer.getModifiedBy() != null) && !(balanceXfer.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", balanceXfer.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE balance_xfers SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(BalanceXfer balanceXfer) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO balance_xfers ("
    		   + "id, "
    		   + "rate, "
    		   + "period, "
    		   + "fee, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :rate, :period, :fee, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("rate", balanceXfer.getRate());
      namedParameters.put("period", balanceXfer.getPeriod());
      namedParameters.put("fee", balanceXfer.getFee());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", balanceXfer.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM balance_xfers WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}