package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.CashAdvance;

import javax.sql.DataSource;
import java.util.List;

public interface CashAdvanceDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Cash Advances table corresponding
    * to a passed cash advance id.
    */
   List<CashAdvance> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Cash Advances table.
    */
   List<CashAdvance> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Cash Advances table.
    */
   void create(CashAdvance cashAdvance);
   
   /** 
    * This is the method to be used to create
    * a record in the Cash Advances table.
    */
   void update(CashAdvance cashAdvance, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Cash Advances table corresponding
    * to a passed cash advance id.
    */
   void deleteById(String id);
}
