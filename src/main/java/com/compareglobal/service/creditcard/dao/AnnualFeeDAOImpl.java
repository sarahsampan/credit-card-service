package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.AnnualFeeMapper;
import com.compareglobal.service.creditcard.model.AnnualFee;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class AnnualFeeDAOImpl implements AnnualFeeDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<AnnualFee> findOne(String id) {
      String SQL = "SELECT * FROM annual_fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<AnnualFee> annualFee = (List<AnnualFee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new AnnualFeeMapper());
      return annualFee;
   }
   
   @Override 
   public List<AnnualFee> findAll() {
	  String SQL = "SELECT * FROM annual_fees";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<AnnualFee> annualFee = (List<AnnualFee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new AnnualFeeMapper());
      return annualFee;
   }
   
   @Override 
   public void update(final AnnualFee annualFee, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((annualFee.getFee() != null) && !(annualFee.getFee().equals(""))) {
    	   updateParams = "fee = :fee, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("fee", annualFee.getFee()); 
       }
       
       if ((annualFee.getSupplementaryCardFee() != null) && !(annualFee.getSupplementaryCardFee().equals(""))) {
    	   updateParams = "supplementary_card_fee = :supplementary_card_fee, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("supplementary_card_fee", annualFee.getSupplementaryCardFee());
       }
       
       if ((annualFee.getWaiver() != null) && !(annualFee.getWaiver().equals(""))) {
    	   updateParams = "waiver = :waiver, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("waiver", annualFee.getWaiver());
       }
       
       if ((annualFee.getModifiedBy() != null) && !(annualFee.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", annualFee.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE annual_fees SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       return;
   }
   
   @Override 
   public void create(AnnualFee annualFee) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO annual_fees ("
    		   + "id, "
    		   + "fee, "
    		   + "supplementary_card_fee, "
    		   + "waiver, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :fee, :supplementary_card_fee, :waiver, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("fee", annualFee.getFee());
      namedParameters.put("supplementary_card_fee", annualFee.getSupplementaryCardFee());
      namedParameters.put("conversion", annualFee.getWaiver());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", annualFee.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM annual_fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}