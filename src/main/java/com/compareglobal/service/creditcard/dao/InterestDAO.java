package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Interest;

import javax.sql.DataSource;
import java.util.List;

public interface InterestDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Interests table corresponding
    * to a passed interest id.
    */
   List<Interest> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Interests table.
    */
   List<Interest> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Fees table.
    */
   void create(Interest interest);
   
   /** 
    * This is the method to be used to create
    * a record in the Interests table.
    */
   void update(Interest interest, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Interests table corresponding
    * to a passed interest id.
    */
   void deleteById(String id);
}
