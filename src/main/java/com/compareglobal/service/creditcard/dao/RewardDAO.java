package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Reward;

import javax.sql.DataSource;
import java.util.List;

public interface RewardDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Rewards table corresponding
    * to a passed reward id.
    */
   List<Reward> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Rewards table.
    */
   List<Reward> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Rewards table.
    */
   void create(Reward reward);
   
   /** 
    * This is the method to be used to create
    * a record in the Rewards table.
    */
   void update(Reward reward, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Rewards table corresponding
    * to a passed reward id.
    */
   void deleteById(String id);
}
