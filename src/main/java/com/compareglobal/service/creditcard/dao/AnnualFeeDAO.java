package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.AnnualFee;

import javax.sql.DataSource;
import java.util.List;

public interface AnnualFeeDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Annual Fees table corresponding
    * to a passed annual fee id.
    */
   List<AnnualFee> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Annual Fees table.
    */
   List<AnnualFee> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Annual Fees table.
    */
   void create(AnnualFee annualFees);
   
   /** 
    * This is the method to be used to create
    * a record in the Annual Fees table.
    */
   void update(AnnualFee annualFees, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Annual Fees table corresponding
    * to a passed annual fee id.
    */
   void deleteById(String id);
}
