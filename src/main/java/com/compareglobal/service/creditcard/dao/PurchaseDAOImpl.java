package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.PurchaseMapper;
import com.compareglobal.service.creditcard.model.Purchase;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class PurchaseDAOImpl implements PurchaseDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<Purchase> findOne(String id) {
      String SQL = "SELECT * FROM purchases WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<Purchase> purchase = (List<Purchase>) namedParameterJdbcTemplate.query(SQL, namedParameters, new PurchaseMapper());
      return purchase;
   }
   
   @Override 
   public List<Purchase> findAll() {
	  String SQL = "SELECT * FROM purchases";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<Purchase> purchase = (List<Purchase>) namedParameterJdbcTemplate.query(SQL, namedParameters, new PurchaseMapper());
      return purchase;
   }
   
   @Override 
   public void update(final Purchase purchase, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((purchase.getRate() != null) && !(purchase.getRate().equals(""))) {
    	   updateParams = "rate = :rate, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("rate", purchase.getRate());
       }
       
       if ((purchase.getPeriod() != null) && !(purchase.getPeriod().equals(""))) {
    	   updateParams = "period = :period, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("period", purchase.getPeriod()); 
       }

       if ((purchase.getModifiedBy() != null) && !(purchase.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", purchase.getModifiedBy()); 
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE links SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Purchase purchase) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO purchases ("
    		   + "id, "
    		   + "rate, "
    		   + "period, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :rate, :period, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("rate", purchase.getRate());
      namedParameters.put("period", purchase.getPeriod());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", purchase.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM purchases WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}