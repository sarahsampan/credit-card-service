package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.OtherFee;

import javax.sql.DataSource;
import java.util.List;

public interface OtherFeeDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Other Fees table corresponding
    * to a passed other fee id.
    */
   List<OtherFee> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Other Fees table.
    */
   List<OtherFee> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Other Fees table.
    */
   void create(OtherFee otherFee);
   
   /** 
    * This is the method to be used to create
    * a record in the Other Fees table.
    */
   void update(OtherFee otherFee, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Other Fees table corresponding
    * to a passed other fee id.
    */
   void deleteById(String id);
}
