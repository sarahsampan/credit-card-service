package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Purchase;

import javax.sql.DataSource;
import java.util.List;

public interface PurchaseDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Purchases table corresponding
    * to a passed purchase id.
    */
   List<Purchase> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Purchases table.
    */
   List<Purchase> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Purchases table.
    */
   void create(Purchase surchase);
   
   /** 
    * This is the method to be used to create
    * a record in the Purchases table.
    */
   void update(Purchase purchase, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Purchases table corresponding
    * to a passed purchase id.
    */
   void deleteById(String id);
}
