package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.InterestMapper;
import com.compareglobal.service.creditcard.model.Interest;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class InterestDAOImpl implements InterestDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public ArrayList<Interest> findOne(String id) {
      String SQL = "SELECT * FROM interests WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      ArrayList<Interest> interest = (ArrayList<Interest>) namedParameterJdbcTemplate.query(SQL, namedParameters, new InterestMapper());
      return interest;
   }
   
   @Override 
   public ArrayList<Interest> findAll() {
	  String SQL = "SELECT * FROM interests";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      ArrayList<Interest> interest = (ArrayList<Interest>) namedParameterJdbcTemplate.query(SQL, namedParameters, new InterestMapper());
      return interest;
   }
   
   @Override 
   public void update(final Interest interest, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((interest.getCreditCardId() != null) && !(interest.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", interest.getCreditCardId());
       }
       
       if ((interest.getBalanceXferId() != null) && !(interest.getBalanceXferId().equals(""))) {
    	   updateParams = "balance_xfer_id = :balance_xfer_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("balance_xfer_id", interest.getBalanceXferId()); 
       }

       if ((interest.getPurchaseId() != null) && !(interest.getPurchaseId().equals(""))) {
    	   updateParams = "purchase_id = :purchase_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("purchase_id", interest.getPurchaseId()); 
       }

       if ((interest.getCashAdvanceId() != null) && !(interest.getCashAdvanceId().equals(""))) {
    	   updateParams = "cash_advance_id = :cash_advance_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("cash_advance_id", interest.getCashAdvanceId()); 
       }
       
       if ((interest.getRepresentative() != null) && !(interest.getRepresentative().equals(""))) {
    	   updateParams = "representative = :representative, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("representative", interest.getRepresentative()); 
       }
       
       if ((interest.getMinimumPayment() != null) && !(interest.getMinimumPayment().equals(""))) {
    	   updateParams = "minimum_payment = :minimum_payment, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("minimum_payment", interest.getMinimumPayment()); 
       }
       
       if ((interest.getLatePaymentCharge() != null) && !(interest.getLatePaymentCharge().equals(""))) {
    	   updateParams = "late_payment_charge = :late_payment_charge, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("late_payment_charge", interest.getLatePaymentCharge()); 
       }

       if ((interest.getModifiedBy() != null) && !(interest.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", interest.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE images SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Interest interest) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO images ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "balance_xfer_id, "
    		   + "purchase_id, "
    		   + "cash_advance_id, "
    		   + "representative, "
    		   + "minimum_payment, "
    		   + "late_payment_charge,"
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :balance_xfer_id, :purchase_id, :cash_advance_id, :representative, :minimum_payment, :late_payment_charge, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", interest.getCreditCardId());
      namedParameters.put("balance_xfer_id", interest.getBalanceXferId());
      namedParameters.put("purchase_id", interest.getPurchaseId());
      namedParameters.put("cash_advance_id", interest.getCashAdvanceId());
      namedParameters.put("representative", interest.getRepresentative());
      namedParameters.put("minimum_payment", interest.getMinimumPayment());
      namedParameters.put("late_payment_charge", interest.getLatePaymentCharge());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", interest.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM interests WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}