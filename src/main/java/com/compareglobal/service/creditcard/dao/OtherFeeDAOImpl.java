package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.OtherFeeMapper;
import com.compareglobal.service.creditcard.model.OtherFee;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class OtherFeeDAOImpl implements OtherFeeDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<OtherFee> findOne(String id) {
      String SQL = "SELECT * FROM other_fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<OtherFee> otherFee = (List<OtherFee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new OtherFeeMapper());
      return otherFee;
   }
   
   @Override 
   public List<OtherFee> findAll() {
	  String SQL = "SELECT * FROM other_fees";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<OtherFee> otherFee = (List<OtherFee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new OtherFeeMapper());
      return otherFee;
   }
   
   @Override 
   public void update(final OtherFee otherFee, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((otherFee.getForeignTransactionFee() != null) && !(otherFee.getForeignTransactionFee().equals(""))) {
    	   updateParams = "foreign_transaction_fee = :foreign_transaction_fee, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("foreign_transaction_fee", otherFee.getForeignTransactionFee());
       }
       
       if ((otherFee.getOverLimit() != null) && !(otherFee.getOverLimit().equals(""))) {
    	   updateParams = "over_limit = :over_limit, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("over_limit", otherFee.getOverLimit()); 
       }

       if ((otherFee.getCardReplacement() != null) && !(otherFee.getCardReplacement().equals(""))) {
    	   updateParams = "card_replacement = :card_replacement, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("card_replacement", otherFee.getCardReplacement()); 
       }

       if ((otherFee.getModifiedBy() != null) && !(otherFee.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", otherFee.getModifiedBy()); 
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE links SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(OtherFee otherFee) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO other_fees ("
    		   + "id, "
    		   + "foreign_transaction_fee, "
    		   + "over_limit, "
    		   + "card_replacement, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :foreign_transaction_fee, :over_limit, :card_replacement, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("foreign_transaction_fee", otherFee.getForeignTransactionFee());
      namedParameters.put("over_limit", otherFee.getOverLimit());
      namedParameters.put("card_replacement", otherFee.getCardReplacement());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", otherFee.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM other_fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}