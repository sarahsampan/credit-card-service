package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.BenefitMapper;
import com.compareglobal.service.creditcard.model.Benefit;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class BenefitDAOImpl implements BenefitDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public ArrayList<Benefit> findOne(String id) {
      String SQL = "SELECT * FROM benefits WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      ArrayList<Benefit> benefit = (ArrayList<Benefit>) namedParameterJdbcTemplate.query(SQL, namedParameters, new BenefitMapper());
      return benefit;
   }
   
   @Override 
   public ArrayList<Benefit> findAll() {
	  String SQL = "SELECT * FROM benefits";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      ArrayList<Benefit> benefit = (ArrayList<Benefit>) namedParameterJdbcTemplate.query(SQL, namedParameters, new BenefitMapper());
      return benefit;
   }
   
   @Override 
   public void update(final Benefit benefit, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((benefit.getCreditCardId() != null) && !(benefit.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", benefit.getCreditCardId());
       }
       
       if ((benefit.getDescription() != null) && !(benefit.getDescription().equals(""))) {
    	   updateParams = "description = :description, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("description", benefit.getDescription()); 
       }
       
       if ((benefit.getModifiedBy() != null) && !(benefit.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", benefit.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE benefits SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Benefit benefit) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO benefits ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "description, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :description, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", benefit.getCreditCardId());
      namedParameters.put("description", benefit.getDescription());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", benefit.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM benefits WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}