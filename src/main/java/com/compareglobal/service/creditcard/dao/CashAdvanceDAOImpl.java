package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.CashAdvanceMapper;
import com.compareglobal.service.creditcard.model.CashAdvance;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class CashAdvanceDAOImpl implements CashAdvanceDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<CashAdvance> findOne(String id) {
      String SQL = "SELECT * FROM cash_advances WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<CashAdvance> cashAdvance = (List<CashAdvance>) namedParameterJdbcTemplate.query(SQL, namedParameters, new CashAdvanceMapper());
      return cashAdvance;
   }
   
   @Override 
   public List<CashAdvance> findAll() {
	  String SQL = "SELECT * FROM cash_advances";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<CashAdvance> cashAdvance = (List<CashAdvance>) namedParameterJdbcTemplate.query(SQL, namedParameters, new CashAdvanceMapper());
      return cashAdvance;
   }
   
   @Override 
   public void update(final CashAdvance cashAdvance, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((cashAdvance.getRate() != null) && !(cashAdvance.getRate().equals(""))) {
    	   updateParams = "rate = :rate, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("rate", cashAdvance.getRate());
       }
       
       if ((cashAdvance.getFee() != null) && !(cashAdvance.getFee().equals(""))) {
    	   updateParams = "fee = :fee, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("fee", cashAdvance.getFee()); 
       }
       
       if ((cashAdvance.getModifiedBy() != null) && !(cashAdvance.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", cashAdvance.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE cash_advances SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(CashAdvance cashAdvance) {
	  UUID uuid = UUID.randomUUID();
      String SQL = "INSERT INTO cash_advances ("
    		   + "id, "
    		   + "rate, "
    		   + "fee, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :rate, :fee, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("rate", cashAdvance.getRate());
      namedParameters.put("fee", cashAdvance.getFee());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", cashAdvance.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM cash_advances WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}