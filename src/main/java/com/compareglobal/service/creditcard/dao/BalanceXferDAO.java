package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.BalanceXfer;

import javax.sql.DataSource;
import java.util.List;

public interface BalanceXferDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Balance Xfers table corresponding
    * to a passed balance xfer id.
    */
   List<BalanceXfer> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Balance Xfers table.
    */
   List<BalanceXfer> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Balance Xfers table.
    */
   void create(BalanceXfer balanceXfer);
   
   /** 
    * This is the method to be used to create
    * a record in the Balance Xfers table.
    */
   void update(BalanceXfer balanceXfer, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Balance Xfers table corresponding
    * to a passed balance xfer id.
    */
   void deleteById(String id);
}
