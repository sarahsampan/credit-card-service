package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.EligibilityMapper;
import com.compareglobal.service.creditcard.model.Eligibility;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class EligibilityDAOImpl implements EligibilityDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public ArrayList<Eligibility> findOne(String id) {
      String SQL = "SELECT * FROM eligibilities WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      ArrayList<Eligibility> eligibility = (ArrayList<Eligibility>) namedParameterJdbcTemplate.query(SQL, namedParameters, new EligibilityMapper());
      return eligibility;
   }
   
   @Override 
   public ArrayList<Eligibility> findAll() {
	  String SQL = "SELECT * FROM cash_advances";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      ArrayList<Eligibility> eligibility = (ArrayList<Eligibility>) namedParameterJdbcTemplate.query(SQL, namedParameters, new EligibilityMapper());
      return eligibility;
   }
   
   @Override 
   public void update(final Eligibility eligibility, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((eligibility.getCreditCardId() != null) && !(eligibility.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", eligibility.getCreditCardId());
       }
       
       if ((eligibility.getAgeMin() != null) && !(eligibility.getAgeMin().equals(""))) {
    	   updateParams = "age_min = :age_min, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("age_min", eligibility.getAgeMin()); 
       }
       
       if ((eligibility.getAgeMax() != null) && !(eligibility.getAgeMax().equals(""))) {
    	   updateParams = "age_max = :age_max";
    	   ((MapSqlParameterSource) namedParameters).addValue("age_max", eligibility.getAgeMax()); 
       }
       
       if ((eligibility.getSupplementaryCard() != null) && !(eligibility.getSupplementaryCard().equals(""))) {
    	   updateParams = "supplementary_card = :supplementary_card";
    	   ((MapSqlParameterSource) namedParameters).addValue("supplementary_card", eligibility.getSupplementaryCard()); 
       }
       
       if ((eligibility.getIncome() != null) && !(eligibility.getIncome().equals(""))) {
    	   updateParams = "income = :income";
    	   ((MapSqlParameterSource) namedParameters).addValue("income", eligibility.getSupplementaryCard()); 
       }

       if ((eligibility.getExistingCardHolder() != null) && !(eligibility.getExistingCardHolder().equals(""))) {
    	   updateParams = "existing_card_holder = :existing_card_holder";
    	   ((MapSqlParameterSource) namedParameters).addValue("existing_card_holder", eligibility.getExistingCardHolder()); 
       }

       if ((eligibility.getModifiedBy() != null) && !(eligibility.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", eligibility.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE eligibilities SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Eligibility eligibility) {
	  UUID uuid = UUID.randomUUID();
      String SQL = "INSERT INTO cash_advances ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "age_min"
    		   + "age_max"
    		   + "supplementary_card"
    		   + "income"
    		   + "existing_card_holder"
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :age_min, :age_max, :supplementary_card, :income, :existing_card_holder, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", eligibility.getCreditCardId());
      namedParameters.put("age_min", eligibility.getAgeMin());
      namedParameters.put("age_max", eligibility.getAgeMax());
      namedParameters.put("supplementary_card", eligibility.getSupplementaryCard());
      namedParameters.put("income", eligibility.getIncome());
      namedParameters.put("existing_card_holder", eligibility.getExistingCardHolder());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", eligibility.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM eligibilities WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}