package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.CreditCardMapper;
import com.compareglobal.service.creditcard.model.CreditCard;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreditCardDAOImpl implements CreditCardDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<CreditCard> findOne(String language, String id) {
      String SQL = "SELECT * FROM credit_cards WHERE id = :id AND language = :language";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);  
      ((MapSqlParameterSource) namedParameters).addValue("language", language);
      List<CreditCard> creditCard = (List<CreditCard>) namedParameterJdbcTemplate.query(SQL, namedParameters, new CreditCardMapper());
      return creditCard;
   }
   
   @Override 
   public List<CreditCard> findAll(String language) {
	  String SQL = "SELECT * FROM credit_cards WHERE language = :language";
	  SqlParameterSource namedParameters = new MapSqlParameterSource("language", language);
      List<CreditCard> creditCard = (List<CreditCard>) namedParameterJdbcTemplate.query(SQL, namedParameters, new CreditCardMapper());
      return creditCard;
   }
   
   @Override 
   public List<CreditCard> compare(String sql) {
      List<CreditCard> creditCard = (List<CreditCard>) namedParameterJdbcTemplate.query(sql, new CreditCardMapper());
      return creditCard;
   }
   @Override 
   public void update(final CreditCard creditCard, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       if ((creditCard.getBrandId() != null) && !(creditCard.getBrandId().equals(""))) {
    	   updateParams = "brand_id = :brand_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("brand_id", creditCard.getBrandId()); 
       }
       if ((creditCard.getName() != null) && !(creditCard.getName().equals(""))) {
    	   updateParams = "name = :name, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("name", creditCard.getName());
       } 
       if ((creditCard.getTags() != null) && !(creditCard.getTags().equals(""))) {
    	   updateParams = "tags = :tags, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("tags", creditCard.getTags());
       }
       if ((creditCard.getLanguage() != null) && !(creditCard.getLanguage().equals(""))) {
    	   updateParams = "language = :language, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("language", creditCard.getLanguage());
       }
       if ((creditCard.getActive() != null) && !(creditCard.getActive().equals(""))) {
    	   updateParams = "active = :active, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("active", creditCard.getActive());
       }      
       if ((creditCard.getModifiedBy() != null) && !(creditCard.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", creditCard.getModifiedBy());
       }
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE credit_cards SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       return;
   }
   
   @Override 
   public void create(CreditCard creditCard) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO credit_cards ("
    		   + "id, "
    		   + "brand_id, "
    		   + "name, "
    		   + "tags, "
    		   + "language, "
    		   + "active, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :brand_id, :name, :tags, :language, :active, :created, :created_by)";
      Map namedParameters = new HashMap(); 
      namedParameters.put("id", uuid.toString());
      namedParameters.put("brand_id", creditCard.getBrandId());
      namedParameters.put("name", creditCard.getName());
      namedParameters.put("tags", creditCard.getTags());
      namedParameters.put("language", creditCard.getLanguage());
      namedParameters.put("active", creditCard.getActive());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", creditCard.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM credit_cards WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}