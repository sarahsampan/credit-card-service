package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.LinkMapper;
import com.compareglobal.service.creditcard.model.Link;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class LinkDAOImpl implements LinkDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<Link> findOne(String id) {
      String SQL = "SELECT * FROM links WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<Link> link = (List<Link>) namedParameterJdbcTemplate.query(SQL, namedParameters, new LinkMapper());
      return link;
   }
   
   @Override 
   public List<Link> findAll() {
	  String SQL = "SELECT * FROM links";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<Link> link = (List<Link>) namedParameterJdbcTemplate.query(SQL, namedParameters, new LinkMapper());
      return link;
   }
   
   @Override 
   public void update(final Link link, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((link.getCreditCardId() != null) && !(link.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", link.getCreditCardId());
       }
       
       if ((link.getApply() != null) && !(link.getApply().equals(""))) {
    	   updateParams = "apply = :apply, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("apply", link.getApply()); 
       }

       if ((link.getInfo() != null) && !(link.getInfo().equals(""))) {
    	   updateParams = "info = :info, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("info", link.getInfo()); 
       }

       if ((link.getProvider() != null) && !(link.getProvider().equals(""))) {
    	   updateParams = "provider = :provider, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("provider", link.getProvider()); 
       }
       
       if ((link.getModifiedBy() != null) && !(link.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", link.getModifiedBy());
       }
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE links SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Link link) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO links ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "apply, "
    		   + "info, "
    		   + "provider, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :apply, :info, :created, :provider, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", link.getCreditCardId());
      namedParameters.put("apply", link.getApply());
      namedParameters.put("info", link.getInfo());
      namedParameters.put("provider", link.getProvider());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", link.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM links WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}