package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.AirMileMapper;
import com.compareglobal.service.creditcard.model.AirMile;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class AirMileDAOImpl implements AirMileDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<AirMile> findOne(String id) {
      String SQL = "SELECT * FROM air_miles WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<AirMile> airMile = (List<AirMile>) namedParameterJdbcTemplate.query(SQL, namedParameters, new AirMileMapper());
      return airMile;
   }
   
   @Override 
   public List<AirMile> findAll() {
	  String SQL = "SELECT * FROM air_miles";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<AirMile> airMile = (List<AirMile>) namedParameterJdbcTemplate.query(SQL, namedParameters, new AirMileMapper());
      return airMile;
   }
   
   @Override 
   public void update(final AirMile airMile, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((airMile.getConditionId() != null) && !(airMile.getConditionId().equals(""))) {
    	   updateParams = "condition_id = :condition_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("condition_id", airMile.getConditionId()); 
       }
       
       if ((airMile.getConversion() != null) && !(airMile.getConversion().equals(""))) {
    	   updateParams = "conversion = :conversion, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("conversion", airMile.getConversion());
       }
       
       if ((airMile.getModifiedBy() != null) && !(airMile.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", airMile.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE air_miles SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       return;
   }
   
   @Override 
   public void create(AirMile airMile) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO air_miles ("
    		   + "id, "
    		   + "condition_id, "
    		   + "conversion, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :condition_id, :conversion, :created, :created_by)";
      Map namedParameters = new HashMap(); 
      namedParameters.put("id", uuid.toString());
      namedParameters.put("condition_id", airMile.getConditionId());
      namedParameters.put("conversion", airMile.getConversion());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", airMile.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM air_miles WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}