package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.FeeMapper;
import com.compareglobal.service.creditcard.model.Fee;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class FeeDAOImpl implements FeeDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public ArrayList<Fee> findOne(String id) {
      String SQL = "SELECT * FROM fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      ArrayList<Fee> fee = (ArrayList<Fee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new FeeMapper());
      return fee;
   }
   
   @Override 
   public ArrayList<Fee> findAll() {
	  String SQL = "SELECT * FROM fees";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      ArrayList<Fee> fee = (ArrayList<Fee>) namedParameterJdbcTemplate.query(SQL, namedParameters, new FeeMapper());
      return fee;
   }
   
   @Override 
   public void update(final Fee fee, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((fee.getCreditCardId() != null) && !(fee.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("fee", fee.getCreditCardId());
       }
       
       if ((fee.getAnnualFeeId() != null) && !(fee.getAnnualFeeId().equals(""))) {
    	   updateParams = "annual_fee_id = :annual_fee_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("annual_fee_id", fee.getAnnualFeeId()); 
       }
       
       if ((fee.getOtherFeeId() != null) && !(fee.getOtherFeeId().equals(""))) {
    	   updateParams = "other_fee_id = :other_fee_id";
    	   ((MapSqlParameterSource) namedParameters).addValue("other_fee_id", fee.getOtherFeeId()); 
       }

       if ((fee.getModifiedBy() != null) && !(fee.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", fee.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE eligibilities SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Fee fee) {
	  UUID uuid = UUID.randomUUID();
      String SQL = "INSERT INTO fees ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "annual_fee_id,"
    		   + "other_fee_id,"
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :credit_card_id, :annual_fee_id, :other_fee_id, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", fee.getCreditCardId());
      namedParameters.put("annual_fee_id", fee.getAnnualFeeId());
      namedParameters.put("other_fee_id", fee.getOtherFeeId());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", fee.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM fees WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}