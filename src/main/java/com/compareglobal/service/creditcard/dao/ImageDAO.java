package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Image;

import javax.sql.DataSource;
import java.util.List;

public interface ImageDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Images table corresponding
    * to a passed image id.
    */
   List<Image> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Images table.
    */
   List<Image> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Images table.
    */
   void create(Image image);
   
   /** 
    * This is the method to be used to create
    * a record in the Images table.
    */
   void update(Image image, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Images table corresponding
    * to a passed image id.
    */
   void deleteById(String id);
}
