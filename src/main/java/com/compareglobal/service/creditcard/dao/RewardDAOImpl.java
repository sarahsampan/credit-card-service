package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.domain.RewardMapper;
import com.compareglobal.service.creditcard.model.Reward;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;

public class RewardDAOImpl implements RewardDAO {
   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
   public void setDataSource(DataSource dataSource) {
      this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
   }
   
   @Override
   public List<Reward> findOne(String id) {
      String SQL = "SELECT * FROM rewards WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource();  
      ((MapSqlParameterSource) namedParameters).addValue("id", id);
      List<Reward> reward = (List<Reward>) namedParameterJdbcTemplate.query(SQL, namedParameters, new RewardMapper());
      return reward;
   }
   
   @Override 
   public List<Reward> findAll() {
	  String SQL = "SELECT * FROM rewards";
	  SqlParameterSource namedParameters = new MapSqlParameterSource();
      List<Reward> reward = (List<Reward>) namedParameterJdbcTemplate.query(SQL, namedParameters, new RewardMapper());
      return reward;
   }
   
   @Override 
   public void update(final Reward reward, final String id) {
	   String updateParams = "";
	   String SQL;
	   
       SqlParameterSource namedParameters = new MapSqlParameterSource();
       
       ((MapSqlParameterSource) namedParameters).addValue("id", id);
       
       if ((reward.getCreditCardId() != null) && !(reward.getCreditCardId().equals(""))) {
    	   updateParams = "credit_card_id = :credit_card_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("credit_card_id", reward.getCreditCardId());
       }
       
       if ((reward.getAirmileId() != null) && !(reward.getAirmileId().equals(""))) {
    	   updateParams = "airmile_id = :airmile_id, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("airmile_id", reward.getAirmileId()); 
       }

       if ((reward.getShopping() != null) && !(reward.getShopping().equals(""))) {
    	   updateParams = "shopping = :shopping, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("shopping", reward.getShopping()); 
       }
       
       if ((reward.getEntertainment() != null) && !(reward.getEntertainment().equals(""))) {
    	   updateParams = "entertainment = :entertainment, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("entertainment", reward.getEntertainment()); 
       }
       
       if ((reward.getModifiedBy() != null) && !(reward.getModifiedBy().equals(""))) {
    	   updateParams = "modified_by = :modified_by, ";
    	   ((MapSqlParameterSource) namedParameters).addValue("modified_by", reward.getModifiedBy());
       }
       
       ((MapSqlParameterSource) namedParameters).addValue("modified", getCurrentDate()); 
       
       SQL =  "UPDATE links SET "
    		    + updateParams
    		    + "modified = :modified "
          		+ "WHERE id = :id"; 
       
       namedParameterJdbcTemplate.update(SQL, namedParameters); 
       
       return;
   }
   
   @Override 
   public void create(Reward reward) {
	  UUID uuid = UUID.randomUUID();
	  
      String SQL = "INSERT INTO rewards ("
    		   + "id, "
    		   + "credit_card_id, "
    		   + "airmile_id, "
    		   + "shopping, "
    		   + "entertainment, "
    		   + "created, "
    		   + "created_by"
    		   + ") "
    		   + "VALUES (:id, :rate, :period, :created, :created_by)";
      Map namedParameters = new HashMap();
      namedParameters.put("id", uuid.toString());
      namedParameters.put("credit_card_id", reward.getCreditCardId());
      namedParameters.put("airmile_id", reward.getAirmileId());
      namedParameters.put("shopping", reward.getShopping());
      namedParameters.put("entertainment", reward.getEntertainment());
      namedParameters.put("created", getCurrentDate());
      namedParameters.put("created_by", reward.getCreatedBy());
      namedParameterJdbcTemplate.update(SQL, namedParameters);
      return;
   }
   
   @Override 
   public void deleteById(String id) {
      String SQL = "DELETE FROM rewards WHERE id = :id";
      SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);  
      namedParameterJdbcTemplate.update(SQL, namedParameters);  
      return;
   }
   
   public String getCurrentDate(){
	   Date dNow = new Date();
	   SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	   return ft.format(dNow);
   }
}