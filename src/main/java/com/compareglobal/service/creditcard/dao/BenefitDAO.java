package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Benefit;

import javax.sql.DataSource;
import java.util.List;

public interface BenefitDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   public void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Benefits table corresponding
    * to a passed benefit id.
    */
   List<Benefit> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Benefits table.
    */
   List<Benefit> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Benefits table.
    */
   void create(Benefit benefit);
   
   /** 
    * This is the method to be used to create
    * a record in the Benefits table.
    */
   void update(Benefit benefit, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Benefits table corresponding
    * to a passed benefit id.
    */
   void deleteById(String id);
}
