package com.compareglobal.service.creditcard.dao;

import com.compareglobal.service.creditcard.model.Link;

import javax.sql.DataSource;
import java.util.List;

public interface LinkDAO {

   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    * @return 
    */
   void setDataSource(DataSource ds);

   /** 
    * This is the method to be used to list down
    * a record from the Links table corresponding
    * to a passed link id.
    */
   List<Link> findOne(String id);
   
   /** 
    * This is the method to be used to list down
    * all the records from the Links table.
    */
   List<Link> findAll();

   /** 
    * This is the method to be used to create
    * a record in the Links table.
    */
   void create(Link link);
   
   /** 
    * This is the method to be used to create
    * a record in the Links table.
    */
   void update(Link link, String id);
   
   /** 
    * This is the method to be used to delete
    * a record from the Links table corresponding
    * to a passed link id.
    */
   void deleteById(String id);
}
