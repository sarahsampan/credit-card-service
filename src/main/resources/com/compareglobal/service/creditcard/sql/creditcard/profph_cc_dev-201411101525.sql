-- MySQL dump 10.13  Distrib 5.5.34, for osx10.6 (i386)
--
-- Host: localhost    Database: profph_cc_dev
-- ------------------------------------------------------
-- Server version	5.5.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airmiles`
--

DROP TABLE IF EXISTS `airmiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airmiles` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `condition_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `conversion` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airmiles`
--

LOCK TABLES `airmiles` WRITE;
/*!40000 ALTER TABLE `airmiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `airmiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annual_fees`
--

DROP TABLE IF EXISTS `annual_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annual_fees` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `fee` decimal(10,0) NOT NULL,
  `supplementary_card_fee` decimal(10,0) NOT NULL,
  `waiver` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annual_fees`
--

LOCK TABLES `annual_fees` WRITE;
/*!40000 ALTER TABLE `annual_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `annual_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balance_xfers`
--

DROP TABLE IF EXISTS `balance_xfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balance_xfers` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `period` int(11) NOT NULL,
  `fee` decimal(10,0) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balance_xfers`
--

LOCK TABLES `balance_xfers` WRITE;
/*!40000 ALTER TABLE `balance_xfers` DISABLE KEYS */;
/*!40000 ALTER TABLE `balance_xfers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `benefits`
--

DROP TABLE IF EXISTS `benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benefits` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benefits`
--

LOCK TABLES `benefits` WRITE;
/*!40000 ALTER TABLE `benefits` DISABLE KEYS */;
/*!40000 ALTER TABLE `benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cash_advances`
--

DROP TABLE IF EXISTS `cash_advances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cash_advances` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `fee` decimal(10,0) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cash_advances`
--

LOCK TABLES `cash_advances` WRITE;
/*!40000 ALTER TABLE `cash_advances` DISABLE KEYS */;
/*!40000 ALTER TABLE `cash_advances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_cards`
--

DROP TABLE IF EXISTS `credit_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_cards` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `tags` enum('featured','exclusive') COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`active`),
  KEY `idx_brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_cards`
--

LOCK TABLES `credit_cards` WRITE;
/*!40000 ALTER TABLE `credit_cards` DISABLE KEYS */;
INSERT INTO `credit_cards` VALUES ('0f6f3e7e-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Rewards Visa','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('11999168-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','PremierMiles Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('1259d0a4-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Shell Citi Classic Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('1334d37a-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Shell Citi Gold Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('14187472-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Rustan\'s Citibank Platinum Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('14da1cda-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Mercury Drug Citi Gold Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('15c08a94-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Rustan\'s Citibank Gold Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('16be648e-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Mercury Drug Citi Classic Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('17ba6806-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Megaworld Citi Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('18c5603e-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Cash Back Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('350afb28-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Gold MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('363b87a6-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Edge MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('37017f6a-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','SkyMiles MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('37bfd56e-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','SkyMiles Platinum MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('3892cc9e-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Petron-BPI MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('39711c74-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Ayala Malls Amore Visa','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('3a4e8b0e-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Ayala Malls Amore Visa Platinum','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('3b5b3c36-3eeb-11e4-9a7a-90a27a7c008a','34e17460-3eeb-11e4-9a7a-90a27a7c008a','Family Credit Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('3c6e21d8-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Platinum VISA','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('3d76556e-3eeb-11e4-9a7a-90a27a7c008a','0d6b45a6-3eea-11e4-9a7a-90a27a7c008a','Rewards MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('7f04c4e8-3eeb-11e4-9a7a-90a27a7c008a','2c0651a0-3d77-11e4-9a7a-90a27a7c008a','Visa Classic','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('7ff5d130-3eeb-11e4-9a7a-90a27a7c008a','2c0651a0-3d77-11e4-9a7a-90a27a7c008a','Visa Gold','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('80bc9018-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','Forever 21 MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('818eed1a-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','Bench MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('826401a8-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','Titanium MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('8331a7b6-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','Platinum MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('840b506a-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','American Express Platinum Credit Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('850ec406-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','American Express Gold Credit Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('86768c16-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','American Express Credit Card','en','featured',1,'2014-09-18 06:20:06','2014-09-18 06:20:06','',''),('877e85d2-3eeb-11e4-9a7a-90a27a7c008a','7229e3fe-3d77-11e4-9a7a-90a27a7c008a','Nickel MasterCard','en','featured',1,'2014-09-18 06:20:06','2014-11-10 10:03:14','','');
/*!40000 ALTER TABLE `credit_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eligibilities`
--

DROP TABLE IF EXISTS `eligibilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eligibilities` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `age_min` int(11) NOT NULL,
  `age_max` int(11) NOT NULL,
  `supplementary_card` int(11) NOT NULL,
  `income` int(11) NOT NULL,
  `existing_card_holder` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eligibilities`
--

LOCK TABLES `eligibilities` WRITE;
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `annual_fee_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `other_fee_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `icon` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interests`
--

DROP TABLE IF EXISTS `interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interests` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `balance_xfer_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `cash_advance_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `representative` decimal(10,0) NOT NULL,
  `minimum_payment` text COLLATE utf8_unicode_ci NOT NULL,
  `late_payment_charge` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interests`
--

LOCK TABLES `interests` WRITE;
/*!40000 ALTER TABLE `interests` DISABLE KEYS */;
/*!40000 ALTER TABLE `interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `apply` text COLLATE utf8_unicode_ci NOT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `provider` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_fees`
--

DROP TABLE IF EXISTS `other_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_fees` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_transaction_fee` text COLLATE utf8_unicode_ci NOT NULL,
  `over_limit` decimal(10,0) NOT NULL,
  `card_replacement` decimal(10,0) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_fees`
--

LOCK TABLES `other_fees` WRITE;
/*!40000 ALTER TABLE `other_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `period` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rewards`
--

DROP TABLE IF EXISTS `rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rewards` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `airmile_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `shopping` text COLLATE utf8_unicode_ci NOT NULL,
  `entertainment` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `modified` datetime NOT NULL DEFAULT '1970-01-01 01:00:01',
  `created_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rewards`
--

LOCK TABLES `rewards` WRITE;
/*!40000 ALTER TABLE `rewards` DISABLE KEYS */;
/*!40000 ALTER TABLE `rewards` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-10 15:25:52
